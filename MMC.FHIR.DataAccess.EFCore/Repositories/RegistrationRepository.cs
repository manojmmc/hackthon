﻿using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.DataAccess.EFCore.Repositories
{
    public class RegistrationRepository : GenericRepository<Registration>, IRegistrationRepository
    {
        public RegistrationRepository(ApplicationContext context) : base(context)
        {

        }
    }
}
