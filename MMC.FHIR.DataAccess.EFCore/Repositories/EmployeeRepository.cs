﻿using Microsoft.EntityFrameworkCore;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace MMC.FHIR.DataAccess.EFCore.Repositories
{
    public class EmployeeRepository : GenericRepository<EmployeeDetails>, IEmployeeRepository
    {
        public EmployeeRepository(ApplicationContext context) : base(context)
        {

        }

        public EmployeeCalimDetails ClaimByClaimId(int id)
        {
            return (from s in _context.Employees
                    join sa in _context.Claims on s.ID equals sa.EmployeeID
                    join emlr in _context.Registration on s.EmployerId equals emlr.ID
                    where sa.ID == id
                    select new EmployeeCalimDetails()
                    {
                        Add1 = s.Add1,
                        Add2 = s.Add2,
                        Add3 = s.Add3,
                        City = s.City,
                        Country = s.Country,
                        EmailID = s.EmailID,
                        EmployerId = s.EmployerId,
                        FirstName = s.FirstName,
                        ID = s.ID,
                        Identifier = s.Identifier,
                        IsPostedonFHIR = s.IsPostedonFHIR,
                        MiddleName = s.MiddleName,
                        MobileNo = s.MobileNo,
                        SurName = s.SurName,
                        ClaimNo = sa.ClaimNo,
                        Status = sa.Status,
                        Priority = sa.Priority,
                        ClaimAmt = sa.ClaimAmt,
                        OrgName = emlr.OrgName,
                        ClaimId = sa.ID,
                        Type = sa.Type,
                        ClaimDate = sa.ClaimDate.GetValueOrDefault()

                    }).FirstOrDefault();
        }

        public IEnumerable<EmployeeCalimDetails> ClaimByEmployeeId(int id)
        {
            return (from s in _context.Employees
                    join sa in _context.Claims on s.ID equals sa.EmployeeID
                    join emlr in _context.Registration on s.EmployerId equals emlr.ID
                    where s.ID == id
                    select new EmployeeCalimDetails()
                    {
                        Add1 = s.Add1,
                        Add2 = s.Add2,
                        Add3 = s.Add3,
                        City = s.City,
                        Country = s.Country,
                        EmailID = s.EmailID,
                        EmployerId = s.EmployerId,
                        FirstName = s.FirstName,
                        ID = s.ID,
                        Identifier = s.Identifier,
                        IsPostedonFHIR = s.IsPostedonFHIR,
                        MiddleName = s.MiddleName,
                        MobileNo = s.MobileNo,
                        SurName = s.SurName,
                        ClaimNo = sa.ClaimNo,
                        Status = sa.Status,
                        Priority = sa.Priority,
                        ClaimAmt = sa.ClaimAmt,
                        OrgName = emlr.OrgName,
                        ClaimId = sa.ID,
                        Type = sa.Type,
                        ClaimDate = sa.ClaimDate.GetValueOrDefault()

                    });
        }

        public IEnumerable<EmployeeCalimDetails> ClaimByEmployerId(int id)
        {
            if (id > 0)
            {
                return (from s in _context.Employees
                        join sa in _context.Claims on s.ID equals sa.EmployeeID
                        join emlr in _context.Registration on s.EmployerId equals emlr.ID
                        where emlr.ID == id
                        select new EmployeeCalimDetails()
                        {
                            Add1 = s.Add1,
                            Add2 = s.Add2,
                            Add3 = s.Add3,
                            City = s.City,
                            Country = s.Country,
                            EmailID = s.EmailID,
                            EmployerId = s.EmployerId,
                            FirstName = s.FirstName,
                            ID = s.ID,
                            Identifier = s.Identifier,
                            IsPostedonFHIR = s.IsPostedonFHIR,
                            MiddleName = s.MiddleName,
                            MobileNo = s.MobileNo,
                            SurName = s.SurName,
                            ClaimNo = sa.ClaimNo,
                            Status = sa.Status,
                            Priority = sa.Priority,
                            ClaimAmt = sa.ClaimAmt,
                            OrgName = emlr.OrgName,
                            ClaimId = sa.ID,
                            Type = sa.Type,
                            ClaimDate = sa.ClaimDate.GetValueOrDefault()

                        });
            }
            else
            {
                return (from s in _context.Employees
                        join sa in _context.Claims on s.ID equals sa.EmployeeID
                        join emlr in _context.Registration on s.EmployerId equals emlr.ID
                        select new EmployeeCalimDetails()
                        {
                            Add1 = s.Add1,
                            Add2 = s.Add2,
                            Add3 = s.Add3,
                            City = s.City,
                            Country = s.Country,
                            EmailID = s.EmailID,
                            EmployerId = s.EmployerId,
                            FirstName = s.FirstName,
                            ID = s.ID,
                            Identifier = s.Identifier,
                            IsPostedonFHIR = s.IsPostedonFHIR,
                            MiddleName = s.MiddleName,
                            MobileNo = s.MobileNo,
                            SurName = s.SurName,
                            ClaimNo = sa.ClaimNo,
                            Status = sa.Status,
                            Priority = sa.Priority,
                            ClaimAmt = sa.ClaimAmt,
                            OrgName = emlr.OrgName,
                            ClaimId = sa.ID,
                            Type = sa.Type,
                            ClaimDate = sa.ClaimDate.GetValueOrDefault()

                        });
            }
        }

        public IEnumerable<EmployeeDetails> GetEmployeeByCount(int count)
        {
            return _context.Employees.OrderByDescending(d => d.ID).Take(count).ToList();
        }

        public EmployeeCalimDetails GetEmployeeClaimData(string claimno, string mob)
        {
            return (from s in _context.Employees
                    join sa in _context.Claims on s.ID equals sa.EmployeeID
                    where sa.ClaimNo == claimno && s.MobileNo == mob
                    select new EmployeeCalimDetails()
                    {
                        Add1 = s.Add1,
                        Add2 = s.Add2,
                        Add3 = s.Add3,
                        City = s.City,
                        Country = s.Country,
                        EmailID = s.EmailID,
                        EmployerId = s.EmployerId,
                        FirstName = s.FirstName,
                        ID = s.ID,
                        Identifier = s.Identifier,
                        IsPostedonFHIR = s.IsPostedonFHIR,
                        MiddleName = s.MiddleName,
                        MobileNo = s.MobileNo,
                        SurName = s.SurName,
                        ClaimNo = sa.ClaimNo,
                        Status = sa.Status,
                        Priority = sa.Priority,
                        ClaimAmt = sa.ClaimAmt

                    }).FirstOrDefault();
        }

        public IEnumerable<EmployeeDetails> GetEmployeeWithEmployerDetail(int employerid)
        {
            if (employerid > 0)
            {
                return (from s in _context.Employees
                        join sa in _context.Registration on s.EmployerId equals sa.ID
                        where s.EmployerId == employerid
                        select new EmployeeDetails()
                        {
                            Add1 = s.Add1,
                            Add2 = s.Add2,
                            Add3 = s.Add3,
                            City = s.City,
                            Country = s.Country,
                            EmailID = s.EmailID,
                            OrgName = sa.OrgName,
                            EmployerId = s.EmployerId,
                            FirstName = s.FirstName,
                            ID = s.ID,
                            Identifier = s.Identifier,
                            IsPostedonFHIR = s.IsPostedonFHIR,
                            MiddleName = s.MiddleName,
                            MobileNo = s.MobileNo,
                            SurName = s.SurName
                        });
            }
            else
            {
                return (from s in _context.Employees
                        join sa in _context.Registration on s.EmployerId equals sa.ID
                        select new EmployeeDetails()
                        {
                            Add1 = s.Add1,
                            Add2 = s.Add2,
                            Add3 = s.Add3,
                            City = s.City,
                            Country = s.Country,
                            EmailID = s.EmailID,
                            OrgName = sa.OrgName,
                            EmployerId = s.EmployerId,
                            FirstName = s.FirstName,
                            ID = s.ID,
                            Identifier = s.Identifier,
                            IsPostedonFHIR = s.IsPostedonFHIR,
                            MiddleName = s.MiddleName,
                            MobileNo = s.MobileNo,
                            SurName = s.SurName
                        });

            }
        }

        public EmployeeDetails GetEmployeeWithEmployerDetailByEmpID(int empID)
        {
            return (from s in _context.Employees
                    join sa in _context.Registration on s.EmployerId equals sa.ID
                    where s.ID == empID
                    select new EmployeeDetails()
                    {
                        Add1 = s.Add1,
                        Add2 = s.Add2,
                        Add3 = s.Add3,
                        City = s.City,
                        Country = s.Country,
                        EmailID = s.EmailID,
                        OrgName = sa.OrgName,
                        EmployerId = s.EmployerId,
                        FirstName = s.FirstName,
                        ID = s.ID,
                        Identifier = s.Identifier,
                        IsPostedonFHIR = s.IsPostedonFHIR,
                        MiddleName = s.MiddleName,
                        MobileNo = s.MobileNo,
                        SurName = s.SurName
                    }).FirstOrDefault();
        }


        public EmployeeDetails GetEmployeeWithEmployerDetailByName(string empFirstName, string employerName)
        {
            return (from s in _context.Employees
                    join sa in _context.Registration on s.EmployerId equals sa.ID
                    where s.FirstName.ToLower() == empFirstName.ToLower() && sa.OrgName.ToLower()== employerName.ToLower()
                    select new EmployeeDetails()
                    {
                        Add1 = s.Add1,
                        Add2 = s.Add2,
                        Add3 = s.Add3,
                        City = s.City,
                        Country = s.Country,
                        EmailID = s.EmailID,
                        OrgName = sa.OrgName,
                        EmployerId = s.EmployerId,
                        FirstName = s.FirstName,
                        ID = s.ID,
                        Identifier = s.Identifier,
                        IsPostedonFHIR = s.IsPostedonFHIR,
                        MiddleName = s.MiddleName,
                        MobileNo = s.MobileNo,
                        SurName = s.SurName
                    }).FirstOrDefault();
        }
    }
}
