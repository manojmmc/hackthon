﻿using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.DataAccess.EFCore.Repositories
{
    public class ClaimRepository : GenericRepository<Claim>, IClaimRepository
    {
        public ClaimRepository(ApplicationContext context) : base(context)
        {

        }
    }
}
