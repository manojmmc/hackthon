﻿using MMC.FHIR.DataAccess.EFCore.Repositories;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.DataAccess.EFCore.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        public IEmployeeRepository Employees { get; private set; }
        public IGenericRepository<Roles> Roles { get; private set; }
        public IRegistrationRepository Registration { get; private set; }
        public IClaimRepository Claim { get; private set; }

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;

            Employees = new EmployeeRepository(_context);
            Roles = new GenericRepository<Roles>(_context);
            Registration = new RegistrationRepository(_context);
            Claim = new ClaimRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
