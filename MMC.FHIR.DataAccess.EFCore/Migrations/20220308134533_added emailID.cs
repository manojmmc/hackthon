﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MMC.FHIR.DataAccess.EFCore.Migrations
{
    public partial class addedemailID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmailID",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailID",
                table: "Employees");
        }
    }
}
