﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MMC.FHIR.DataAccess.EFCore.Migrations
{
    public partial class NotificationRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsNotified",
                table: "Registration");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsNotified",
                table: "Registration",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
