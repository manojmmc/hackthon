﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MMC.FHIR.DataAccess.EFCore.Migrations
{
    public partial class restructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Registration_EmployeeAddresses_AddressID",
                table: "Registration");

            migrationBuilder.DropTable(
                name: "EmployeeAddresses");

            migrationBuilder.DropIndex(
                name: "IX_Registration_AddressID",
                table: "Registration");

            migrationBuilder.DropColumn(
                name: "AddressID",
                table: "Registration");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Registration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Employees",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Registration");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Employees");

            migrationBuilder.AddColumn<int>(
                name: "AddressID",
                table: "Registration",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EmployeeAddresses",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddressLine1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressLine2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressLine3 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmployeeID = table.Column<int>(type: "int", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeAddresses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_EmployeeAddresses_Employees_EmployeeID",
                        column: x => x.EmployeeID,
                        principalTable: "Employees",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Registration_AddressID",
                table: "Registration",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeAddresses_EmployeeID",
                table: "EmployeeAddresses",
                column: "EmployeeID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Registration_EmployeeAddresses_AddressID",
                table: "Registration",
                column: "AddressID",
                principalTable: "EmployeeAddresses",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
