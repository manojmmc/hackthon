﻿using Microsoft.EntityFrameworkCore;
using MMC.FHIR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.DataAccess.EFCore
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
        public DbSet<EmployeeDetails> Employees { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Registration>  Registration  { get; set; }
        public DbSet<Claim> Claims { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Roles>()
                .HasData(
                    new Roles
                    {
                        ID = 1,
                        RoleName = "Admin"
                    },
                    new Roles
                    {
                        ID = 2,
                        RoleName = "Employer"
                    },
                     new Roles
                     {
                         ID = 3,
                         RoleName = "Insurar"
                     },
                      new Roles
                      {
                          ID = 4,
                          RoleName = "Employee"
                      }
                );
        }
    }
}
