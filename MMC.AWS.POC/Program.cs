﻿using System;
using System.Net.Http;

namespace MMC.AWS.POC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            CheckAWSConnect();
        }

        public static void CheckAWSConnect()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    Console.WriteLine("Trying to access hosted api ");
                    //var resp=httpClient.GetAsync("http://www.google.com").Result // this works perfect!                   
                    var resp = httpClient.GetAsync("https://snvavutjhe.execute-api.us-west-2.amazonaws.com/dev").Result.Content.ReadAsStringAsync().Result; // this throws error
                    Console.WriteLine($"Call was successful- {resp}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception from main function " + ex.Message);
                Console.WriteLine(ex.InnerException.Message);
                Console.WriteLine(ex.StackTrace);

            }
        }
    }
}
