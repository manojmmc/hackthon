﻿namespace MMC.FHIR.EntityConverter
{
    public interface IConverter<T, S>
    {
        S From(T obj);
        T To(S obj);
 
    }



}
