﻿using Hl7.Fhir.Model;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Models;
using System;
using System.Collections.Generic;

namespace MMC.FHIR.EntityConverter
{
    public class EmployeeEntityConverter : IEmployeeEntityConverter<Patient, EmployeeDetails>
    {
        public EmployeeEntityConverter()
        {

        }
        public EmployeeDetails From(Patient obj)
        {
            return new EmployeeDetails();
        }

        public Patient To(EmployeeDetails obj)
        {
            return new Patient()
            {
                Address = GetFHIRAddresses(obj),

                Identifier = Identifier("Test", obj.Identifier),
                Id = obj.Identifier,
                BirthDate = "",
                MaritalStatus = new CodeableConcept() { Text = "" },
                Name = GetHumanNames(obj)
            };
        }

        private List<Identifier> Identifier(string system, string value)
        {
            var ret = new List<Identifier>();
            ret.Add(GetIdentifier(system, value));
            return ret;
        }

        private List<HumanName> GetHumanNames(EmployeeDetails obj)
        {
            var ret = new List<HumanName>();
            ret.Add(GetHumanName(obj));
            return ret;
        }

        private Identifier GetIdentifier(string system, string value)
        {
            return new Identifier()
            {
                System = system,
                Value = value
            };
        }

        private HumanName GetHumanName(EmployeeDetails obj)
        {
            return new HumanName()
            {
                Given = GetHumanGivenName(obj)
            };
        }

        private IEnumerable<string> GetHumanGivenName(EmployeeDetails obj)
        {
            var ret = new List<string>();
            ret.Add(obj.FirstName);
            ret.Add(obj.MiddleName);
            return ret;
        }


        private List<Address> GetFHIRAddresses(EmployeeDetails emp)
        {
            var add = new List<Address>();
            add.Add(GetFHIRAddress(emp));
            return add;

        }

        private Address GetFHIRAddress(EmployeeDetails emp)
        {
            if (emp != null)
            {
                return new Address()
                {
                    Line = GetFHIRLineAddress(emp),
                    City = emp.City,
                    Country = emp.Country
                };
            }
            return null;
        }

        private IEnumerable<string> GetFHIRLineAddress(EmployeeDetails address)
        {
            var line = new List<string>();
            if (address != null)
            {
                line.Add(address.Add1);
                line.Add(address.Add2);
                line.Add(address.Add3);
            }
            return line;
        }

    }

}
