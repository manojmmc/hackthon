﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace MMC.POC
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = testMethodForPosting();
            if (result)
            {
                Console.WriteLine("Success");
            }
        }

        public static bool testMethodForPosting()
        {
            var envVariable = "http://mmcfhirapi-dev.us-west-2.elasticbeanstalk.com/";
            Console.WriteLine($" CallApiToCheckInput envVariable-{envVariable}");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(envVariable);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //var obj = new { OrderId = orderId, Ids = ids };
                //string json = "{\"key\"=" + slots[FIELDNAME] + ",\"value\"=" + slots[FIELDVALUE] + ",\"firstName\"=" + slots[FIRSTNAME] + ",\"mob\"=" + slots[MOBILENO] + " }";
                var key = "lastname";
                var val = "from app";
                var firstName = "manoj";
                var mob = "12345";
                var json = "{\"key\": \"" + key + "\",\"value\": \"" + val + "\",\"firstName\": \"" + firstName + "\",\"mob\": \"" + mob + "\"}";
                //PutModel obj = new PutModel();
                //obj.key = "lastname";
                //obj.value = "2132324";
                //obj.firstName = "manoj";
                //obj.mob = "234324";
                Console.WriteLine($"TO update profile put json-{json}");
                HttpContent postContent = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PutAsync("api/Employee", postContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success of proceedToUpdateProfile");
                }
            }
            return true;
        }
        public static bool testMethod()
        {
            var envVariable = "http://mmcfhirapi-dev.us-west-2.elasticbeanstalk.com/";
            Console.WriteLine($" CallApiToCheckInput envVariable-{envVariable}");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(envVariable);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method
                HttpResponseMessage response = client.GetAsync($"api/Employee/getempbynameandmob?fname=manoj&mob=12345").Result;
                if (response.IsSuccessStatusCode)
                {
                    var returnObject = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine($"Able to find record. returnObject is : - {returnObject}");
                    if (returnObject != null && returnObject != "")
                    {
                        return true;
                    }
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return false; ;

        }
    }
}
