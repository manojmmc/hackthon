﻿using System;

namespace MMC.FHIR.Models.Configuration
{
    public class TwilioConfiguration
    {
        public const string SectionName = "Twilio";
        public string AccountSid { get; set; } = String.Empty;
        public string AuthToken { get; set; } = String.Empty;
        public string PhoneNumber { get; set; } = String.Empty;
    }
}
