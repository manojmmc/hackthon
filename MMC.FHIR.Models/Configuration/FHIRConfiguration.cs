﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Models.Configuration
{
    public class FHIRConfiguration
    {
        public const string SectionName = "FHIR";

        public string Server { get; set; } = String.Empty;
    }
}
