﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Models
{

    public enum UserRole
    {
        Admin,
        Employee

    }
    public class User
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }

    }



    //public class Employee
    //{
    //    public string Identifier { get; set; }
    //    public string FirstName { get; set; }
    //    public string MiddleName { get; set; }
    //    public string SurName { get; set; }

    //    public Employee Address { get; set; }

    //}

    //public class Employee
    //{
    //    public string Add1 { get; set; }
    //    public string Add2 { get; set; }
    //    public string Add3 { get; set; }

    //    public string City { get; set; }
    //    public string Country { get; set; }
    //}
}
