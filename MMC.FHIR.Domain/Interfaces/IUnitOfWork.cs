﻿using MMC.FHIR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IEmployeeRepository Employees { get; }
        IGenericRepository<Roles> Roles { get; }
        IRegistrationRepository Registration { get; }
        IClaimRepository Claim { get; }
        int Complete();
    }
}
