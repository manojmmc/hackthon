﻿using MMC.FHIR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Interfaces
{
    public interface IRegistrationRepository : IGenericRepository<Registration>
    {
    }
}
