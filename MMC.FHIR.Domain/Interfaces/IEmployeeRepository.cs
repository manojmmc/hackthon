﻿using MMC.FHIR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Interfaces
{
    public interface IEmployeeRepository : IGenericRepository<EmployeeDetails>
    {
        IEnumerable<EmployeeDetails> GetEmployeeByCount(int count);
        IEnumerable<EmployeeDetails> GetEmployeeWithEmployerDetail(int employerId);

        EmployeeCalimDetails GetEmployeeClaimData(string claimno, string mob);
        EmployeeCalimDetails ClaimByClaimId(int id);
        IEnumerable<EmployeeCalimDetails> ClaimByEmployeeId(int id);
        IEnumerable<EmployeeCalimDetails> ClaimByEmployerId(int id);
        EmployeeDetails GetEmployeeWithEmployerDetailByEmpID(int empID);
        EmployeeDetails GetEmployeeWithEmployerDetailByName(string empFirstName, string employerName);
    }
}
