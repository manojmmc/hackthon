﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Entities
{
    public class Roles
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
    }
}
