﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Entities
{
    public class EmployeeDetails
    {
        public int ID { get; set; }
        public string Identifier { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public string OrgName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public int EmployerId { get; set; }
        public bool IsPostedonFHIR { get; set; }

        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string Add3 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
    public class EmployeeCalimDetails: EmployeeDetails
    {
        public int ClaimId { get; set; }
        public string ClaimNo { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public decimal ClaimAmt { get; set; }
        public string Type { get; set; }
        public DateTime ClaimDate { get; set; }

    }

}
