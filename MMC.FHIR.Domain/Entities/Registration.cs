﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Entities
{
    public class Registration
    {
        public int ID { get; set; }
        public string OrgName { get; set; }
        public string Address { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string MobileNo { get; set; }
        public int RoleID { get; set; }
        public bool Status { get; set; } = true;
    }
    public class RegistrationViewModel : Registration
    {
        public string RoleName { get; set; }

        public RegistrationViewModel ConvertToViewModel(Registration a)
        {
            return new RegistrationViewModel()
            {
                ID = a.ID,
                OrgName = a.OrgName,
                EmailID = a.EmailID,
                Address = a.Address,
                MobileNo = a.MobileNo,
                RoleID = a.RoleID,
                Status = a.Status,
                RoleName = GetRoleName(a.RoleID)

            };
        }

        private string GetRoleName(int roleID)
        {
            string roleName = "";
            switch (roleID)
            {
                case 1:
                    roleName = "Admin";
                    break;
                case 2:
                    roleName = "Employer";
                    break;
                case 3:
                    roleName = "Insurar";
                    break;
                case 4:
                    roleName = "Insurar";
                    break;

                default:
                    throw  new Exception("Role not found");
            }
            return roleName;
        }
    }
}
