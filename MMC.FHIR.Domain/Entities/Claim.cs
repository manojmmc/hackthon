﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMC.FHIR.Domain.Entities
{
    public class Claim
    {
        public int ID { get; set; }
        public string ClaimNo { get; set; }
        public decimal ClaimAmt { get; set; }
        public int EmployeeID { get; set; }
        public int EmployerID { get; set; }
        public string Status { get; set; } = "New";
        public string Type { get; set; } = "Dental";
        public string SubType { get; set; }
        public string Use { get; set; }
        public string Priority { get; set; } = "Normal";
        public string FundsReserve { get; set; }
        public string RelatedRelationship { get; set; }
        public string PayeeType { get; set; }

        public DateTime? ClaimDate { get; set; }
    }

    public class ClaimRequest
    {
        public int id { get; set; }
        public string ClaimNo { get; set; }
        public decimal ClaimAmt { get; set; }

        public DateTime ClaimDate { get; set; }
        public int EmployeeID { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Priority { get; set; }

        public Claim GetPostModel()
        {
            return new Claim()
            {
                ClaimAmt = this.ClaimAmt,
                ClaimNo = this.ClaimNo,
                EmployeeID = this.EmployeeID,
                Priority = this.Priority,
                Status = this.Status,
                Type = this.Type,
                ClaimDate=this.ClaimDate,
                ID=this.id
            };

        }

    }
}
