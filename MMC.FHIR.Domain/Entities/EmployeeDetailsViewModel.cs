﻿namespace MMC.FHIR.Domain.Entities
{
    public class EmployeeDetailsViewModel : EmployeeDetails
    {
        public EmployeeDetailsViewModel ConvertToViewModel(EmployeeDetails a)
        {
            return new EmployeeDetailsViewModel()
            {
                ID = a.ID,
                OrgName = a.OrgName,
                EmailID = a.EmailID,
                MobileNo = a.MobileNo,
                FirstName = a.FirstName,
                MiddleName = a.MiddleName,
                SurName = a.SurName,
                Add1 = a.Add1,
                Add2 = a.Add2,
                Add3 = a.Add3,
                City = a.City,
                Country = a.Country,
                EmployerId = a.EmployerId,
                IsPostedonFHIR = a.IsPostedonFHIR,
                Identifier = a.Identifier
            };
        }
    }
}
