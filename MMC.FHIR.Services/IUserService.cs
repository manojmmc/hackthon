﻿using MMC.FHIR.Models;
using System.Threading.Tasks;

namespace MMC.FHIR.Services
{
    public interface IUserService
    {
        public Task<User> GetUserData(int v);
        public Task<User> AuthorizeUser(string username, string password);
    }

}
