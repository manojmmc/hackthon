﻿using MMC.FHIR.Models;
using System.Threading.Tasks;

namespace MMC.FHIR.Services
{
    public class UserService : IUserService
    {
        public UserService()
        {

        }

        public async Task<User> AuthorizeUser(string username, string password)
        {
            await System.Threading.Tasks.Task.Delay(500);
            return new User() { UserId = "manoj", Username = "manoj", Role = UserRole.Admin };
        }

        public async Task<User> GetUserData(int v)
        {
            await System.Threading.Tasks.Task.Delay(500);
            return new User() { UserId = "manoj", Role = UserRole.Admin };
        }
    }

}
