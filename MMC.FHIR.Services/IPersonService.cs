﻿using Hl7.Fhir.Model;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Models;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MMC.FHIR.Services
{
    public interface IPersonService
    {
        public Task<EmployeeDetails> GetPersonData(int v);
        Task<Patient> PostDataToFHIRServer(EmployeeDetails pData);
        public Task<Patient> GetDataFromFHIRServer(string identifier);
    }
}
