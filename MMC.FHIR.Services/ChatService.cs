﻿using Microsoft.Extensions.Options;
using MMC.FHIR.Models.Configuration;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MMC.FHIR.Services
{
    public class ChatService : IChatService
    {
        private readonly TwilioConfiguration _options;
        public ChatService(IOptions<TwilioConfiguration> options)
        {
            _options = options.Value;
        }

        public async Task<bool> AddCallerID(string phoneno)
        {
            TwilioClient.Init(_options.AccountSid, _options.AuthToken);

            var validationRequest = await ValidationRequestResource.CreateAsync(
                friendlyName: "Test phone number",
                phoneNumber: new Twilio.Types.PhoneNumber(phoneno)
            );

            string code = validationRequest.ValidationCode;

            return true;// validationRequest.ValidationCode
        }
    }
}
