﻿using System.Threading.Tasks;

namespace MMC.FHIR.Services
{
    public interface IChatService
    {
        public Task<bool> AddCallerID(string phoneno);
    }
}
