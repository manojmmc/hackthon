﻿using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Hl7.Fhir.Serialization;
using Microsoft.Extensions.Options;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.EntityConverter;
using MMC.FHIR.Models;
using MMC.FHIR.Models.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIR.Services
{
    public class PersonService : IPersonService
    {
        private readonly FHIRConfiguration _options;
        private readonly IEmployeeEntityConverter<Patient, EmployeeDetails> _entityConverter;
        public PersonService(IOptions<FHIRConfiguration> options, IEmployeeEntityConverter<Patient, EmployeeDetails> entityConverter)
        {
            _options = options.Value;
            _entityConverter = entityConverter;
        }

        public async Task<EmployeeDetails> GetPersonData(int v)
        {
            var test = _options.Server;
            return new EmployeeDetails();
        }

        public async Task<Patient> GetDataFromFHIRServer(string identifier)
        {
            List<Patient> patients = new List<Patient>();
            using (var client = new FhirClient(_options.Server))
            {

                Bundle results = await client.SearchAsync<Patient>(new string[]
                {
                    "identifier="+identifier
                });

                foreach (Patient resource in results.GetResources())
                {
                    patients.Add(resource);
                }
            }
            return patients.LastOrDefault();
        }

        private string PrettyPrint(Patient patient)
        {
            var serializer = new FhirJsonSerializer(new SerializerSettings()
            {
                Pretty = true
            });

            return serializer.SerializeToString(patient);
        }

        public async Task<Patient> PostDataToFHIRServer(EmployeeDetails pData)
        {
            using (var client = new FhirClient(_options.Server))
            {
                var pat = _entityConverter.To(pData);
                return await client.UpdateAsync<Patient>(pat);
            }
        }


    }
}
