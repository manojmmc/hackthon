﻿using Amazon.Lambda.Core;
using Amazon.Lambda.LexEvents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using static MMC.AWS.LexbotUpdateProfileLambdaFunction.FlowerOrder;

namespace MMC.AWS.LexbotUpdateProfileLambdaFunction
{

    public class ClaimResult
    {
        public string ClaimNo { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public decimal ClaimAmt { get; set; }

    }
    public class EmployeeDetails
    {
        public int ID { get; set; }
        public string Identifier { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public string OrgName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public int EmployerId { get; set; }
        public bool IsPostedonFHIR { get; set; }

        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string Add3 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }

    public class CheckFHIRIntentProcessor : AbstractIntentProcessor
    {
      
        public const string FIRSTNAME = "firstname";
        public const string EMPLOYERNAME = "employername";
        public override LexResponse Process(LexEvent lexEvent, ILambdaContext context)
        {
            IDictionary<string, string> slots = lexEvent.CurrentIntent.Slots;
            IDictionary<string, string> sessionAttributes = lexEvent.SessionAttributes ?? new Dictionary<string, string>();

            //if all the values in the slots are empty return the delegate, theres nothing to validate or do.
            if (slots.All(x => x.Value == null))
            {
                return Delegate(sessionAttributes, slots);
            }
            //if (slots[FIRSTNAME] != null && slots[EMPLOYERNAME] != null)
            //{
            //    Console.WriteLine("if of FIRSTNAME and FIRSTNAME");
            //    var result = Helper.CheckAndGetBackendData(slots[FIRSTNAME], slots[MOBILENO]);
            //    if (!result.IsValid)
            //    {
            //        Console.WriteLine($"result.IsValid - {result.IsValid.ToString()}");
            //        slots[FIRSTNAME] = null;
            //        slots[EMPLOYERNAME] = null;
            //        return ElicitUpdateProfileSlot(sessionAttributes, lexEvent.CurrentIntent.Name, slots, FIRSTNAME, result.Message);
            //    }
            //}
            if (string.Equals(lexEvent.InvocationSource, "DialogCodeHook", StringComparison.Ordinal))
            {
                if (slots.Any(x => x.Value == null))
                {
                    return Delegate(sessionAttributes, slots);
                }
                var result = GetFHIRData(slots);
                string msg = string.Empty;
                if (result == null)
                {
                    msg = $"Sorry we are unable to fetch record for {slots[FIRSTNAME]} now.Please try again";
                }
                else
                {
                    msg = $"We found data and status of posting data on server is {result.IsPostedonFHIR} for {result.FirstName} {result.SurName}";
                }
                return Close(
                   sessionAttributes,
                   "Fulfilled",
                   new LexResponse.LexMessage
                   {
                       ContentType = MESSAGE_CONTENT_TYPE,
                       Content = String.Format($"Thanks for contacting us. Please find response . {msg}")
                   }
               );
            }

            return Close(
                  sessionAttributes,
                  "Fulfilled",
                  new LexResponse.LexMessage
                  {
                      ContentType = MESSAGE_CONTENT_TYPE,
                      Content = String.Format("Thanks for contacting us.We would like for server you again. See you again at MMC hackathon")
                  }
              );
        }

        private EmployeeDetails GetFHIRData(IDictionary<string, string> slots)
        {
            EmployeeDetails retClaimObject = null;
            if (slots.All(x => x.Value != null))
            {
                var envVariable = Environment.GetEnvironmentVariable("requestApiUrl");
                Console.WriteLine($"envVariable-{envVariable}");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(envVariable);
                    client.DefaultRequestHeaders.Accept.Clear();
                    //HttpContent postContent = new StringContent(json, Encoding.UTF8, "application/json");
                    //HttpResponseMessage response = client.PutAsync("api/Employee", postContent).Result;
                    var empfirstname = Convert.ToString(slots[FIRSTNAME]);
                    var employerName = Convert.ToString(slots[EMPLOYERNAME]);
                    HttpResponseMessage response = client.GetAsync($"api/Employee/getempfhirstatus?fname={empfirstname}&employername={employerName}").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //returnMsg =  response.Content.ReadAsStringAsync().Result;
                        var responseContent = response.Content.ReadAsStringAsync().Result;
                        retClaimObject = JsonConvert.DeserializeObject<EmployeeDetails>(responseContent);
                        Console.WriteLine($"Success of GetClaimData with result - {responseContent}");
                    }
                }
            }
            return retClaimObject;
        }
    }

    public class CheckClaimIntentProcessor : AbstractIntentProcessor
    {
        public const string MOBILENO = "mobilenumber";
        public const string FIRSTNAME = "firstname";
        public const string CLAIMNO = "claimno";
        public override LexResponse Process(LexEvent lexEvent, ILambdaContext context)
        {
            IDictionary<string, string> slots = lexEvent.CurrentIntent.Slots;
            IDictionary<string, string> sessionAttributes = lexEvent.SessionAttributes ?? new Dictionary<string, string>();

            //if all the values in the slots are empty return the delegate, theres nothing to validate or do.
            if (slots.All(x => x.Value == null))
            {
                return Delegate(sessionAttributes, slots);
            }
            if (slots[FIRSTNAME] != null && slots[MOBILENO] != null && slots[CLAIMNO] == null)
            {
                Console.WriteLine("if of FIRSTNAME and FIRSTNAME");
                var result = Helper.CheckAndGetBackendData(slots[FIRSTNAME], slots[MOBILENO]);
                if (!result.IsValid)
                {
                    Console.WriteLine($"result.IsValid - {result.IsValid.ToString()}");
                    slots[FIRSTNAME] = null;
                    slots[MOBILENO] = null;
                    return ElicitUpdateProfileSlot(sessionAttributes, lexEvent.CurrentIntent.Name, slots, FIRSTNAME, result.Message);
                }
            }
            if (string.Equals(lexEvent.InvocationSource, "DialogCodeHook", StringComparison.Ordinal))
            {
                if (slots.Any(x => x.Value == null))
                {
                    return Delegate(sessionAttributes, slots);
                }
                var result = GetClaimData(slots);
                string msg = string.Empty;
                if (result == null)
                {
                    msg = $"Sorry we are unable to fetch claim {slots[CLAIMNO]} data now.Please try again";
                }
                else
                {
                    msg = $"Claim with claim number {result.ClaimNo} and claim amount {result.ClaimAmt} have current status {result.Status}";
                }
                return Close(
                   sessionAttributes,
                   "Fulfilled",
                   new LexResponse.LexMessage
                   {
                       ContentType = MESSAGE_CONTENT_TYPE,
                       Content = String.Format($"Thanks for contacting us. Please find response . {msg}")
                   }
               );
            }

            return Close(
                  sessionAttributes,
                  "Fulfilled",
                  new LexResponse.LexMessage
                  {
                      ContentType = MESSAGE_CONTENT_TYPE,
                      Content = String.Format("Thanks for contacting us.We would like for server you again. See you again at MMC hackathon")
                  }
              );
        }

        private ClaimResult GetClaimData(IDictionary<string, string> slots)
        {
            ClaimResult retClaimObject = null;
            if (slots.All(x => x.Value != null))
            {
                var envVariable = Environment.GetEnvironmentVariable("requestApiUrl");
                Console.WriteLine($"envVariable-{envVariable}");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(envVariable);
                    client.DefaultRequestHeaders.Accept.Clear();
                    //HttpContent postContent = new StringContent(json, Encoding.UTF8, "application/json");
                    //HttpResponseMessage response = client.PutAsync("api/Employee", postContent).Result;
                    var claimno = Convert.ToString(slots[CLAIMNO]);
                    var mobileNo = Convert.ToString(slots[MOBILENO]);
                    HttpResponseMessage response = client.GetAsync($"api/Employee/GetClaimData?claimno={claimno}&mob={mobileNo}").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //returnMsg =  response.Content.ReadAsStringAsync().Result;
                        var responseContent = response.Content.ReadAsStringAsync().Result;
                        retClaimObject = JsonConvert.DeserializeObject<ClaimResult>(responseContent);
                        Console.WriteLine($"Success of GetClaimData with result - {responseContent}");
                    }
                }
            }
            return retClaimObject;
        }
    }

    public class UpdateProfileIntentProcessor : AbstractIntentProcessor
    {
        public const string EMAIL = "Email";
        public const string MOBILENO = "mobilenumber";
        public const string FIRSTNAME = "firstname";
        public const string FIELDNAME = "fieldName";
        public const string FIELDVALUE = "fieldValue";
        FieldKeys _chosenFieldKey = FieldKeys.Null;


        public override LexResponse Process(LexEvent lexEvent, ILambdaContext context)
        {
            IDictionary<string, string> slots = lexEvent.CurrentIntent.Slots;
            IDictionary<string, string> sessionAttributes = lexEvent.SessionAttributes ?? new Dictionary<string, string>();

            //if all the values in the slots are empty return the delegate, theres nothing to validate or do.
            if (slots.All(x => x.Value == null))
            {
                return Delegate(sessionAttributes, slots);
            }

            //if the field name has a value, validate that it is contained within the enum list available.
            if (slots[FIELDNAME] != null)
            {
                var validateFieldResult = ValidateFieldName(slots[FIELDNAME]);

                if (!validateFieldResult.IsValid)
                {
                    slots[validateFieldResult.ViolationSlot] = null;
                    return ElicitSlot(sessionAttributes, lexEvent.CurrentIntent.Name, slots, validateFieldResult.ViolationSlot, validateFieldResult.Message);
                }
            }

            if (slots[FIRSTNAME] != null && slots[MOBILENO] != null && slots[FIELDNAME] == null && slots[FIELDVALUE] == null)
            {
                Console.WriteLine("if of FIRSTNAME and FIRSTNAME");
                var result = CheckAndGetBackendData(slots[FIRSTNAME], slots[MOBILENO]);
                if (!result.IsValid)
                {
                    Console.WriteLine($"result.IsValid - {result.IsValid.ToString()}");
                    slots[FIRSTNAME] = null;
                    slots[MOBILENO] = null;
                    return ElicitUpdateProfileSlot(sessionAttributes, lexEvent.CurrentIntent.Name, slots, FIRSTNAME, result.Message);
                }
            }
            if (string.Equals(lexEvent.InvocationSource, "DialogCodeHook", StringComparison.Ordinal))
            {
                Console.WriteLine("if of DialogCodeHook block");
                if (slots.Any(x => x.Value == null))
                {
                    return Delegate(sessionAttributes, slots);
                }
                else
                {
                    proceedToUpdateProfile(slots);
                    return Close(
                       sessionAttributes,
                       "Fulfilled",
                       new LexResponse.LexMessage
                       {
                           ContentType = MESSAGE_CONTENT_TYPE,
                           Content = String.Format("Thanks, your data has been update successfully")
                       }
                   );
                }
            }
            return Close(
                       sessionAttributes,
                       "Fulfilled",
                       new LexResponse.LexMessage
                       {
                           ContentType = MESSAGE_CONTENT_TYPE,
                           Content = String.Format("Thanks for contacting us.We would like for server you again. See you again at MMC hackathon")
                       }
                   );

        }

        private void proceedToUpdateProfile(IDictionary<string, string> slots)
        {
            if (slots.All(x => x.Value != null))
            {
                var envVariable = Environment.GetEnvironmentVariable("requestApiUrl");
                Console.WriteLine($"envVariable-{envVariable}");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(envVariable);
                    client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //var obj = new { OrderId = orderId, Ids = ids };
                    //string json = "{\"key\"=" + slots[FIELDNAME] + ",\"value\"=" + slots[FIELDVALUE] + ",\"firstName\"=" + slots[FIRSTNAME] + ",\"mob\"=" + slots[MOBILENO] + " }";

                    //var json = "{\"key\": " + slots[FIELDNAME] + ",\"value\": " + slots[FIELDVALUE] + ",\"firstName\": " + slots[FIRSTNAME] + ",\"mob\": " + slots[MOBILENO] + "}";
                    var json = "{\"key\": \"" + slots[FIELDNAME] + "\",\"value\": \"" + slots[FIELDVALUE] + "\",\"firstName\": \"" + slots[FIRSTNAME] + "\",\"mob\": \"" + slots[MOBILENO] + "\"}";
                    Console.WriteLine($"TO update profile put json-{json}");
                    HttpContent postContent = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PutAsync("api/Employee", postContent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Success of proceedToUpdateProfile");
                    }
                }
            }
        }

        private ValidationResult ValidateFieldName(string fieldName)
        {
            bool isFlowerTypeValid = Enum.IsDefined(typeof(FieldKeys), fieldName.ToUpper());
            //TODO- change it
            if (fieldName.ToLower() == "firstname" || fieldName.ToLower() == "first name"
                || fieldName.ToLower() == "lastname" || fieldName.ToLower() == "last name"
                || fieldName.ToLower() == "middlename" || fieldName.ToLower() == "middle name"
                || fieldName.ToLower() == "email"
                )
            {
                return ValidationResult.VALID_RESULT;
            }
            else
            {
                return new ValidationResult(false, FIELDNAME, String.Format("you do not have permission to update the field {0}, please provide correct field name", fieldName));
            }
        }

        private ValidationResult CheckAndGetBackendData(string email, string mobile)
        {
            var result = CallApiToCheckInput(email, mobile);
            if (result)
            {
                //_chosenFlowerType = (FlowerTypes)flowerType;
                return ValidationResult.VALID_RESULT;
            }
            else
            {
                return new ValidationResult(false, "", String.Format("Sorry but we are unable to fetch your data in our system ."));
            }
        }

        private bool CallApiToCheckInput(string firstName, string mobile)
        {
            var envVariable = Environment.GetEnvironmentVariable("requestApiUrl");
            Console.WriteLine($" CallApiToCheckInput envVariable-{envVariable}");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(envVariable);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method
                HttpResponseMessage response = client.GetAsync($"api/Employee/getempbynameandmob?fname={firstName}&mob={mobile}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var returnObject = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine($"Able to find record. returnObject is : - {returnObject}");
                    if (returnObject != null && returnObject != "")
                    {
                        return true;
                    }
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return false;
        }
    }

}
