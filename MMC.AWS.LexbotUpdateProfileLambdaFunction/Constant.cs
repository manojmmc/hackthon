﻿
using Amazon.Lambda.Core;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MMC.AWS.LexbotUpdateProfileLambdaFunction
{
    public class Constant
    {
        public class IntentName
        {
            public const string UpdateProfile = "UpdateProfile";
            public const string CheckClaimStatus = "CheckClaimStatus";
            public const string FHIRStatus = "FHIRStatus";
        }
    
    }

    public static class Helper
    {
        public static ValidationResult CheckAndGetBackendData(string email, string mobile)
        {
            var result = CallApiToCheckInput(email, mobile);
            if (result)
            {
                //_chosenFlowerType = (FlowerTypes)flowerType;
                return ValidationResult.VALID_RESULT;
            }
            else
            {
                return new ValidationResult(false, "", String.Format("Sorry but we are unable to fetch your data in our system ."));
            }
        }

        private static bool CallApiToCheckInput(string firstName, string mobile)
        {
            var envVariable = Environment.GetEnvironmentVariable("requestApiUrl");
            Console.WriteLine($" CallApiToCheckInput envVariable-{envVariable}");
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(envVariable);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method
                HttpResponseMessage response = client.GetAsync($"api/Employee/getempbynameandmob?fname={firstName}&mob={mobile}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var returnObject = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine($"Able to find record. returnObject is : - {returnObject}");
                    if (returnObject != null && returnObject != "")
                    {
                        return true;
                    }
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return false;
        }

    }
}
