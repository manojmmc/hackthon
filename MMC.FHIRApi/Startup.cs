using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MMC.FHIR.DataAccess.EFCore;
using MMC.FHIR.DataAccess.EFCore.Repositories;
using MMC.FHIR.DataAccess.EFCore.UnitOfWork;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using MMC.FHIR.EntityConverter;
using MMC.FHIR.Models;
using MMC.FHIR.Models.Configuration;
using MMC.FHIR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi
{
    public class Startup
    {
        public readonly string allowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<FHIRConfiguration>(Configuration.GetSection(FHIRConfiguration.SectionName));
            services.Configure<TwilioConfiguration>(Configuration.GetSection(TwilioConfiguration.SectionName));
            services.AddScoped<IEmployeeEntityConverter<Patient, EmployeeDetails>, EmployeeEntityConverter>();
            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IUserService, UserService>();
            
            services.AddControllers();
            services.AddDbContext<ApplicationContext>(options => {
                options.UseSqlServer(
                    Configuration.GetConnectionString("SqlConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationContext).Assembly.FullName));
            });
            services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IRegistrationRepository, RegistrationRepository>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IChatService, ChatService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
           

            services.AddCors(o => o.AddPolicy(allowSpecificOrigins, builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(allowSpecificOrigins);
            app.UseAuthorization();


            app.UseSwaggerUI(options =>
            {
#if DEBUG
                // For Debug in Kestrel
                options.SwaggerEndpoint("swagger/v1/swagger.json", "Web API V1");
#else
               options.SwaggerEndpoint("swagger/v1/swagger.json", "Web API V1");
#endif
                options.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
