﻿using Microsoft.AspNetCore.Mvc;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using MMC.FHIRApi.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public LoginController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public IActionResult Login(LoginModel requestMpdel)
        {
            var registration = _unitOfWork.Registration.GetAll().Where(c => c.EmailID.ToUpper() == requestMpdel.Username.ToUpper() && c.Password == requestMpdel.Password).SingleOrDefault();
            return Ok(registration);
        }
    }


}
