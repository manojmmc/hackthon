﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMC.FHIR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly ILogger<ChatController> _logger;
        private readonly IChatService _chatService;
        public ChatController(ILogger<ChatController> logger, IChatService chatService)
        {
            _logger = logger;
            _chatService = chatService;
        }

        [HttpGet]
        [Route("AddCallerID")]
        public async Task<bool> AddCallerID(string phonenumber)
        {
            return await _chatService.AddCallerID(phonenumber);

            //return true;
            //// Find your Account SID and Auth Token at twilio.com/console
            //// and set the environment variables. See http://twil.io/secure
            //string accountSid = Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID");
            //string authToken = Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN");

            //TwilioClient.Init(accountSid, authToken);

            //var validationRequest = ValidationRequestResource.Create(
            //    friendlyName: "My Home Phone Number",
            //    phoneNumber: new Twilio.Types.PhoneNumber("+14158675310")
            //);

            //Console.WriteLine(validationRequest.FriendlyName);
        }


        [HttpGet]
        [Route("SendMessage")]
        public async Task<bool> SendMsg(string phonenumber)
        {
            return true;
        }
    }
}
