﻿using Microsoft.AspNetCore.Mvc;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public EmployeeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        public IActionResult GetEmployee()
        {
            var employees = _unitOfWork.Employees.GetEmployeeWithEmployerDetail(0);
            return Ok(employees);
        }

        [HttpGet]
        [Route("GetClaimData")]
        public IActionResult GetClaimData(string claimno, string mob)
        {
            var result = _unitOfWork.Employees.GetEmployeeClaimData(claimno, mob);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetClaimDataByID/{id:int}")]
        public IActionResult GetClaimDataByID(int id)
        {
            var result = _unitOfWork.Employees.ClaimByClaimId(id);
            return Ok(result);
        }
        [HttpGet("{id:int}")]
        public IActionResult GetEmployee(int id)
        {
            var employees = _unitOfWork.Employees.GetEmployeeWithEmployerDetailByEmpID(id);
            return Ok(employees);
        }

        [HttpGet]
        [Route("getempfhirstatus")]
        public IActionResult GetEmployeeByFNameAndEmployer(string fname, string employername)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.GetEmployeeWithEmployerDetailByName(fname, employername);
            return Ok(employees);
        }


        [HttpGet]
        [Route("getempbynameandmob")]
        public IActionResult GetEmployeeByFNameAndMobile(string fname, string mob)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.Find(a => a.FirstName.ToLower() == fname && a.MobileNo == mob).FirstOrDefault();
            return Ok(employees);
        }


        [HttpGet]
        [Route("ClaimByEmployeeId/{id:int}")]
        public IActionResult GetClaimByEmployeeId(int id)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.ClaimByEmployeeId(id);
            return Ok(employees);
        }

        [HttpGet]
        [Route("ClaimByEmployeerId/{id:int}")]
        public IActionResult ClaimByEmployeerId(int id)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.ClaimByEmployerId(id);
            return Ok(employees);
        }


        [HttpGet]
        [Route("EmployeeByMobileNo")]
        public IActionResult GetEmployeeByMobile(string mob)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.Find(a => a.MobileNo == mob).FirstOrDefault();
            return Ok(employees);
        }

        [HttpGet]
        [Route("AllEmployeByEmployerId/{id:int}")]
        public IActionResult GetAllEmployeByEmployerId(int id)
        {
            var employees = _unitOfWork.Employees.GetEmployeeWithEmployerDetail(id);
            var result = new List<EmployeeDetailsViewModel>();
            if (employees != null)
            {
                foreach (var a in employees)
                {
                    result.Add(new EmployeeDetailsViewModel().ConvertToViewModel(a)); ;
                }
            }
            return Ok(result);
        }


        [HttpPost]
        [Route("AddEmployeeDetailInBulk")]
        public IActionResult AddEmployeeDetailInBulk(List<EmployeeDetails> employeeDetails)
        {
            foreach (EmployeeDetails employee in employeeDetails)
            {
                employee.Identifier = Guid.NewGuid().ToString();
                employee.IsPostedonFHIR = false;

                _unitOfWork.Employees.Add(employee);
                _unitOfWork.Registration.Add(new Registration
                {
                    OrgName = employee.OrgName,
                    EmailID = employee.EmailID,
                    MobileNo = employee.MobileNo,
                    Password = "1234",
                    RoleID = 4,
                    Status = true
                });
            }
            _unitOfWork.Complete();
            return Ok();
        }


        [HttpPut]
        public IActionResult UpdateEmployeeDetail(PutModel data)
        {
            // TODO- add mobile condition 
            var employees = _unitOfWork.Employees.Find(a => a.FirstName.ToLower() == data.firstName.ToLower() && a.MobileNo == data.mob).FirstOrDefault();
            if (employees != null)
            {
                if (data.key.ToLower() == "firstname" || data.key.ToLower() == "first name")
                {
                    employees.FirstName = data.value;
                }
                if (data.key.ToLower() == "lastname" || data.key.ToLower() == "last name")
                {
                    employees.SurName = data.value;
                }
                if (data.key.ToLower() == "email")
                {
                    employees.EmailID = data.value;
                }
                if (data.key.ToLower() == "middlename" || data.key.ToLower() == "middle name")
                {
                    employees.MiddleName = data.value;
                }
                _unitOfWork.Employees.Update(employees);
                _unitOfWork.Complete();
                return Ok();
            }
            var error = new { status = "error", Message = $"Employe record not found for emp:{data.firstName}  and mobile : {data.mob}" };
            return BadRequest(error);
        }

        [HttpPost]
        public IActionResult AddEmployeeDetail(EmployeeDetails employeeDetails)
        {
            if (employeeDetails.EmployerId == 0)
            {
                var error = new { status = "error", Message = "Employer id cant be null" };
                return BadRequest(error);
            }
            var data = _unitOfWork.Employees.Find(a => a.EmailID == employeeDetails.EmailID || a.MobileNo == employeeDetails.MobileNo);
            if (data != null && data.Count() > 0)
            {
                var error = new { status = "error", Message = "Email id or Mobile no found. Please change it" };
                return BadRequest(error);
            }
            employeeDetails.Identifier = Guid.NewGuid().ToString();
            employeeDetails.IsPostedonFHIR = false;
            _unitOfWork.Employees.Add(employeeDetails);
            _unitOfWork.Registration.Add(new Registration
            {
                OrgName = employeeDetails.OrgName,
                EmailID = employeeDetails.EmailID,
                Password = "1234",
                RoleID = 4,
                Status = true

            });
            _unitOfWork.Complete();
            return Ok(employeeDetails);
        }
    }

    public class PutModel
    {
        public string key { get; set; }
        public string value { get; set; }
        public string firstName { get; set; }
        public string mob { get; set; }
    }
}
