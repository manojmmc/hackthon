﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMC.FHIR.Models;
using MMC.FHIR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }


        //[HttpPost]
        //[Route("authenticate")]
        //public async Task<string> AuthenticateUser()
        //{
        //    await System.Threading.Tasks.Task.Delay(500);
        //    return "Hello world";

        //}

        [HttpPost]
        [Route("authenticate")]
        public async Task<ActionResult<User>> AuthenticateUser(User user)
        {
            return await _userService.AuthorizeUser(user.Username, user.Password);
            //return CreatedAtAction("GetProducts", new { id = products.ProductId }, products);
        }
    }
}
