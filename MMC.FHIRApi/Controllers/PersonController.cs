﻿using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using MMC.FHIR.Models;
using MMC.FHIR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly IPersonService _personService;
        private readonly IUnitOfWork _unitOfWork;


        public PersonController(ILogger<PersonController> logger, IPersonService personService, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _personService = personService;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("TestApi")]
        public async Task<string> TestApiMethod()
        {
            await System.Threading.Tasks.Task.Delay(500);
            return "Hello world";

        }

        [HttpGet]
        [Route("GetPersonFHIR/{empID:int}")]
        public async Task<Patient> GetPersonDataFromFHIR(int empID)
        {
            var employees = _unitOfWork.Employees.Find(a => a.ID == empID).FirstOrDefault();
            if (employees != null)
            {
                return await _personService.GetDataFromFHIRServer(employees.Identifier);
            }
            else
            {
                return null;
            }

        }
        [HttpGet]
        [Route("PersonDataToFHIR/{empID}")]
        public async Task<Patient> PersonDataToFHIR(string empID)
        {
            if (string.IsNullOrEmpty(empID))
            {
                return null;
            }
            var numbers = empID.Split(',').Select(Int32.Parse).ToList();
            var employees = _unitOfWork.Employees.Find(a => numbers.Contains(a.ID));
            Patient returndata = null;
            foreach (var employee in employees)
            {
                returndata = await _personService.PostDataToFHIRServer(employee);
                if (returndata != null)
                {
                    employee.IsPostedonFHIR = true;
                }
                _unitOfWork.Employees.Update(employee);
            }
            _unitOfWork.Complete();
            return returndata;

        }
    }
}
