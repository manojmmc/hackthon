﻿using Microsoft.AspNetCore.Mvc;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public RegistrationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult GetAllRegistration()
        {
            var registration = _unitOfWork.Registration.GetAll();
            return Ok(registration);
        }

        [HttpGet]
        [Route("GetAllAdmins")]
        public IActionResult GetAllAdmins()
        {
            var registration = _unitOfWork.Registration.Find(a => a.RoleID == 2 || a.RoleID == 3);
            var result = new List<RegistrationViewModel>();
            foreach (var a in registration)
            {
                result.Add(new RegistrationViewModel().ConvertToViewModel(a)); ;
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllAdminsByRoleId/{roleId:int}/{userid:int}")]
        public IActionResult GetAllAdminsByRoleId(int roleId, int userid)
        {
            dynamic registration = null;
            if (roleId == 1)
            {
                registration = _unitOfWork.Registration.Find(a => a.RoleID == 2 || a.RoleID == 3);
            }
            else
            {
                registration = _unitOfWork.Registration.Find(a => a.ID == userid);
            }
            var result = new List<RegistrationViewModel>();
            foreach (var a in registration)
            {
                result.Add(new RegistrationViewModel().ConvertToViewModel(a)); ;
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllEmployess")]
        public IActionResult GetAllEmployess()
        {
            var registration = _unitOfWork.Registration.Find(a => a.RoleID == 4);
            var result = new List<RegistrationViewModel>();
            foreach (var a in registration)
            {
                result.Add(new RegistrationViewModel().ConvertToViewModel(a)); ;
            }
            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public IActionResult GetRegistrationByID(int id)
        {
            var registration = _unitOfWork.Registration.GetByID(id);
            return Ok(registration);
        }

        [HttpPost]
        public IActionResult AddRegistrationDetail(Registration registration)
        {
            var data = _unitOfWork.Registration.Find(a => a.EmailID == registration.EmailID);
            if (data != null && data.Count() > 0)
            {
                var error = new { status = "error", Message = "email id found" };
                return BadRequest(error);
            }
            _unitOfWork.Registration.Add(registration);
            _unitOfWork.Complete();
            return Ok();
        }
        [HttpPut("{id}")]
        public IActionResult UpdateRegistration(int id, Registration registration)
        {
            if (id != registration.ID)
            {
                return BadRequest();
            }

            _unitOfWork.Registration.Update(registration);
            _unitOfWork.Complete();
            return Ok();
        }
    }
}
