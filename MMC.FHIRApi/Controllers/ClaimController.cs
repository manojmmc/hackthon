﻿using Microsoft.AspNetCore.Mvc;
using MMC.FHIR.Domain.Entities;
using MMC.FHIR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMC.FHIRApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClaimController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public ClaimController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult GetClaim()
        {
            var claim = _unitOfWork.Claim.GetAll();
            return Ok(claim);
        }

        [HttpGet("{employeeID:int}")]
        public IActionResult GetClaimByEmployeeID(int employeeID)
        {
            var claim = _unitOfWork.Claim.GetAll().Where(c => c.EmployeeID == employeeID).SingleOrDefault();
            return Ok(claim);
        }

        [HttpPost]
        public IActionResult AddClaim(ClaimRequest claim)
        {
            if (claim.EmployeeID == 0)
            {
                var error = new { status = "error", Message = "Employee id cant be blank" };
                return BadRequest(error);
            }
            // check emp exits with id
            var empdata = _unitOfWork.Employees.Find(a => a.ID == claim.EmployeeID).FirstOrDefault();
            if (empdata == null)
            {
                var error = new { status = "error", Message = $"Employee not found with id:{claim.EmployeeID}" };
                return BadRequest(error);
            }
            // For update 
            if (claim.id > 0)
            {
                var cdata = _unitOfWork.Claim.Find(a => a.ID == claim.id).FirstOrDefault();
                if (cdata == null)
                {
                    var error = new { status = "error", Message = $"claim not found with id:{claim.id}" };
                    return BadRequest(error);
                }
                // check claim no
                var cclaimnoCheck = _unitOfWork.Claim.Find(a => a.ClaimNo == claim.ClaimNo && a.ID!= claim.id);
                if (cclaimnoCheck != null && cclaimnoCheck.Count() > 0)
                {
                    var error = new { status = "error", Message = $"Claim no - {claim.ClaimNo} already assigned. Please change it. " };
                    return BadRequest(error);
                }
                cdata.ClaimAmt = claim.ClaimAmt;
                cdata.ClaimDate = claim.ClaimDate;
                cdata.ClaimNo = claim.ClaimNo;
                cdata.Priority = claim.Priority;
                cdata.Status = claim.Status;
                cdata.Type = claim.Type;
                _unitOfWork.Claim.Update(cdata);
            }
            else
            {
                //check claim no should be unique
                var data = _unitOfWork.Claim.Find(a => a.ClaimNo == claim.ClaimNo);
                if (data != null && data.Count() > 0)
                {
                    var error = new { status = "error", Message = $"Claim no- {claim.ClaimNo} found" };
                    return BadRequest(error);
                }
                _unitOfWork.Claim.Add(claim.GetPostModel());
            }
            _unitOfWork.Complete();
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateClaim(Claim claim)
        {
            _unitOfWork.Claim.Update(claim);
            _unitOfWork.Complete();

            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteClaim(Claim claim)
        {
            _unitOfWork.Claim.Remove(claim);
            _unitOfWork.Complete();

            return Ok();
        }
    }
}
